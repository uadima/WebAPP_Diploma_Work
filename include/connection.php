<?php
if (!file_exists("mydb.db")) {
    $db= new SQLite3('mydb.db');
    $sql = "CREATE TABLE users(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            username TEXT NOT NULL UNIQUE,
            email TEXT NOT NULL,
            password TEXT);";
    $db->query($sql);
    $sql = "INSERT INTO users (username, email, password) VALUES ('admin','admin@mail.com','password');";
    $db->query($sql);
    unset($sql);
    $sql = "INSERT INTO users (username, email, password) VALUES ('user','user@mail.com','userpassword');";
    $db->query($sql);
} else {
    $db = new SQLite3('mydb.db');
}
php ?>
